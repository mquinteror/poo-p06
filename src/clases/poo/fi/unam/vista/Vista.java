package clases.poo.fi.unam.vista;

public class Vista {
    private String mensaje;

    public Vista (){
	this.mensaje = "Hola, mensaje enviado desde la calse [ Vista ]";
    }
    
    public void setMensaje( String mensaje ){
	this.mensaje = mensaje;
    }
    public String getMensaje(){
	return this.mensaje;
    }
    
}
