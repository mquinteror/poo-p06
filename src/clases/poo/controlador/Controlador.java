package clases.poo.controlador;

public class Controlador {
    private String mensaje;

    public Controlador(){
	this.mensaje = "Hola, mensaje enviado desde la clase [ Controlador] ";
    }

    public void setMensaje( String mensaje ){
	this.mensaje = mensaje;
    }

    public String getMensaje(  ){
	return this.mensaje;
    }
}
