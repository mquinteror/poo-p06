package clases.modelo;

public class Modelo {
    private String mensaje;

    public Modelo (){
	this.mensaje = "Hola, mensaje enviado desde la clase [ Modelo ]";
    }

    public void setMensaje( String mensaje ){
	this.mensaje = mensaje;
    }
    public String getMensaje(){
	return this.mensaje;
    }

}
