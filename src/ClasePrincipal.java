import clases.poo.fi.unam.vista.Vista;
import clases.modelo.Modelo;
import clases.poo.controlador.Controlador;

class ClasePrincipal {
    public static void main ( String[] srgs ){
	System.out.println("Hola, este mensaje es enviado desde la clase [ ClasePrincipal ]");
	/* instanciacin de las demas clases  */
	Modelo model = new Modelo();
	Vista view = new Vista();
	Controlador controller = new Controlador();
	/* impresion del mensaje contenido en las clases instanciadas */
	System.out.println( model.getMensaje() );
	System.out.println( view.getMensaje() );
	System.out.println( controller.getMensaje() );
    }
}
